package au.com.modis.etg.datacenter.transformer;

public interface OutboundProperty {
    String CORELATIONID = "JMSSPCorelationID";
    String MESSAGEID = "JMSSPMessageID";
}
