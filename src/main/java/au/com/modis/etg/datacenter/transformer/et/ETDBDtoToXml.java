package au.com.modis.etg.datacenter.transformer.et;

import java.io.IOException;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import au.com.modis.etg.common.utils.ZipUtil;
import au.com.modis.etg.db.dto.DBDtoBase;

/**
 * SIRI ET content has NOT been zipped.
 * @author nicholaszhu
 *
 */
public class ETDBDtoToXml extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		DBDtoBase resp = (DBDtoBase) message.getPayload();
		message.setPayload(resp.getZip());
		return message;
	}

}
