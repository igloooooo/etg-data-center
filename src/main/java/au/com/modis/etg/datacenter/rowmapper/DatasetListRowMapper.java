package au.com.modis.etg.datacenter.rowmapper;

import java.math.BigDecimal;
import java.util.Map;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import au.com.modis.etg.datacenter.api.dto.DatasetListDto;

public class DatasetListRowMapper implements Callable {
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		DatasetListDto sb = new DatasetListDto();
		Map map = (Map) eventContext.getMessage().getPayload();
		for (Object name : map.keySet()) {
			Object value = map.get(name);
			if (name.toString().equalsIgnoreCase(RowColumnConstants.DATASET_NAME)) {
				sb.setName((String) value);
			} else if (name.toString().equalsIgnoreCase(RowColumnConstants.DATASET_COUNT)) {
				sb.setCount(((BigDecimal) value).intValue());
			}
		}
		return sb;
	}
}
