package au.com.modis.etg.datacenter.transformer.vm;

import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.joda.time.DateTime;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;
import org.w3c.dom.Node;

import com.quintiq.serviceplanner.trainlocationadvice.TExtensionVehicleActivity;

import au.com.modis.etg.common.utils.ETGDBDateUtil;
import uk.org.siri.siri.MonitoredCallStructure;
import uk.org.siri.siri.Siri;
import uk.org.siri.siri.VehicleActivityStructure;
import uk.org.siri.siri.VehicleMonitoringDeliveryStructure;

/**
 * this class will convert VMEsDto to VM message It also will change the target
 * date
 * 
 * @author nicholaszhu
 *
 */
public class VMDBDtoToVMMessage extends AbstractMessageTransformer {

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		Siri siri = (Siri) message.getPayload();
		DateTime targetDate = ETGDBDateUtil.parseFromDB(message.getProperty("targetDate", PropertyScope.SESSION));
		// start to change the date
		siri.getServiceDelivery().setResponseTimestamp(
				ETGDBDateUtil.convertToTargetDate(siri.getServiceDelivery().getResponseTimestamp(), targetDate));
		VehicleMonitoringDeliveryStructure vehicleMonitorDelivery = siri.getServiceDelivery().getVehicleMonitoringDelivery().get(0);
		vehicleMonitorDelivery.setResponseTimestamp(
				ETGDBDateUtil.convertToTargetDate(vehicleMonitorDelivery.getResponseTimestamp(), targetDate));
		VehicleActivityStructure vehicleActivity = vehicleMonitorDelivery.getVehicleActivity().get(0);
		vehicleActivity.setRecordedAtTime(
				ETGDBDateUtil.convertToTargetDate(vehicleActivity.getRecordedAtTime(), targetDate));
		vehicleActivity.setValidUntilTime(
				ETGDBDateUtil.convertToTargetDate(vehicleActivity.getValidUntilTime(), targetDate));
		MonitoredCallStructure monitoredCall = vehicleActivity.getMonitoredVehicleJourney().getMonitoredCall();
		if (monitoredCall.getExpectedArrivalTime() != null ) {
			monitoredCall.setExpectedArrivalTime(
					ETGDBDateUtil.convertToTargetDate(monitoredCall.getExpectedArrivalTime(), targetDate));
		}
		if (monitoredCall.getActualDepartureTime() != null ) {
			monitoredCall.setActualDepartureTime(
					ETGDBDateUtil.convertToTargetDate(monitoredCall.getActualDepartureTime(), targetDate));
		}
		// extension part
		if (vehicleActivity.getExtensions().getAny() != null) {
			TExtensionVehicleActivity extension = (TExtensionVehicleActivity)((JAXBElement)vehicleActivity.getExtensions().getAny().get(0)).getValue();
			if (extension.getTripStartDate() != null) {
				extension.setTripStartDate(
						ETGDBDateUtil.convertToTargetDate(extension.getTripStartDate(), targetDate));	
			}	
		}

		
		message.setPayload(siri);
		return message;
	}

}
