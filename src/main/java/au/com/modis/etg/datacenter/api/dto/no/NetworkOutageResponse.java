package au.com.modis.etg.datacenter.api.dto.no;

import java.util.List;

public class NetworkOutageResponse {
	public List<NOItem> getNoItems() {
		return noItems;
	}

	public void setNoItems(List<NOItem> noItems) {
		this.noItems = noItems;
	}

	List<NOItem> noItems;
	
}
