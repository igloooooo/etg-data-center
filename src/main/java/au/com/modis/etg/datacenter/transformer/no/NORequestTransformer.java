package au.com.modis.etg.datacenter.transformer.no;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;

import com.quintiq.serviceplanner.possessionsresp.PossessionsRequestMsg;

import au.com.modis.etg.callback.dto.NORequestDto;
import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.transformer.SessionVars;

public class NORequestTransformer extends AbstractMessageTransformer {

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		PossessionsRequestMsg msg = (PossessionsRequestMsg) message.getPayload();
		NORequestDto noReq = new NORequestDto();
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
		noReq.setRequestDate(fmt.print(ETGDBDateUtil
				.convertFromXml(msg.getBody().getPossessionsRequest().getPossessionsDateRange().getStartDateTime())));
		message.setPayload(noReq);
		// set requestRef as session var
		message.setProperty(SessionVars.REQUEST_REF, msg.getHeader().getMsgID(), PropertyScope.SESSION);
		return message;
	}

}
