package au.com.modis.etg.datacenter.email;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;

import au.com.modis.etg.common.utils.DateTimeUtils;

public class PTGEmailContent extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		String fileName = message.getProperty("originalFilename", PropertyScope.INBOUND);
		String date = (String)message.getProperty("dataset", PropertyScope.OUTBOUND);
		String borderName = (String)message.getProperty("borderName", PropertyScope.OUTBOUND);
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		String now = fmt.print(DateTime.now());
		message.setPayload(
				"This email was produced by the ROC ETG Project. \n"
				+ "Attached is train graph for " 
				+ borderName + " Board for " + date + " and was created on " + now + ".\n\n\n\n");
		return message;
	}

}
