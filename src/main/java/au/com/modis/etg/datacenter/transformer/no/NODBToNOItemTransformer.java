package au.com.modis.etg.datacenter.transformer.no;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;
import org.python.google.common.base.Optional;

import com.quintiq.serviceplanner.possessionsresp.PossessionsResponseMsg;

import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.api.dto.no.NOItem;
import au.com.modis.etg.datacenter.transformer.SessionVars;

/**
 * this response is for visualize of playback: generate NOItem 
 * @author nicholaszhu
 *
 */
public class NODBToNOItemTransformer extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		final Optional<DateTime> targetDate = Optional.fromNullable(message.getProperty(SessionVars.TARGET_DATE, PropertyScope.SESSION)).transform(dateStr -> {
			return ETGDBDateUtil.parseFromDB((String)dateStr);
		});
		final Optional<DateTime> baselineDate = Optional.fromNullable(message.getProperty(SessionVars.BASELINE_DATE, PropertyScope.SESSION)).transform(dateStr -> {
			return ETGDBDateUtil.parseFromDB((String)dateStr);
		});
		
		Integer diffDays = targetDate.isPresent() ? Days.daysBetween(new LocalDate(targetDate.get()), new LocalDate(baselineDate.get())).getDays() : 0;
		
		PossessionsResponseMsg msg = ((PossessionsResponseMsg) message.getPayload());
		List<NOItem> list = msg.getBody().getPossessionsResponse().getOutages().getOutage().stream().map(outage -> {
			DateTime approvedStartDateTime = ETGDBDateUtil.convertFromXml(outage.getApprovedStartDateTime());
			DateTime approvedEndDateTime = ETGDBDateUtil.convertFromXml(outage.getApprovedEndDateTime());
			DateTime requestedStartDateTime = ETGDBDateUtil.convertFromXml(outage.getRequestedStartDateTime());
			DateTime requestedEndDateTime = ETGDBDateUtil.convertFromXml(outage.getRequestedEndDateTime());
			
			List<NOItem> noItemList = outage.getOutageLinks().getOutageLink().stream().map(outageLink -> {
				NOItem noItem = new NOItem();
				noItem.setApprovedStartDateTime(approvedStartDateTime.minusDays(diffDays));
				noItem.setApprovedEndDateTime(approvedEndDateTime.minusDays(diffDays));
				noItem.setRequestedStartDateTime(requestedStartDateTime.minusDays(diffDays));
				noItem.setRequestedEndDateTime(requestedEndDateTime.minusDays(diffDays));
				noItem.setComments(outage.getComments());
				noItem.setName(outage.getName());
				noItem.setId(outage.getId());
				noItem.setFromNode(outageLink.getFromNode());
				noItem.setToNode(outageLink.getToNode());
				return noItem;	
			}).collect(Collectors.toList());
			return noItemList;
		}).flatMap(List::stream).collect(Collectors.toList());
		Set<NOItem> temp = new HashSet<NOItem>();
		temp.addAll(list);
		message.setPayload(new ArrayList<NOItem>(temp));
		return message;
	}

}
