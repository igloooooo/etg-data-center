package au.com.modis.etg.datacenter.transformer.et;

import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBElement;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;
import org.python.google.common.base.Optional;

import com.quintiq.serviceplanner.siri.TrainServiceInformation;

import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.api.dto.pt.PTCallItem;
import au.com.modis.etg.datacenter.api.dto.pt.PTTripItem;
import au.com.modis.etg.datacenter.constants.TripDirection;
import au.com.modis.etg.datacenter.transformer.SessionVars;
import au.com.modis.etg.datacenter.util.ETGDateConvertUtil;
import uk.org.siri.siri.ServiceDelivery;
import uk.org.siri.siri.Siri;

public class ETDBToETItemTransformer extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		Siri siri = ((Siri) message.getPayload());
		ServiceDelivery sd = siri.getServiceDelivery();
		final Optional<DateTime> targetDate = Optional.fromNullable(message.getProperty(SessionVars.TARGET_DATE, PropertyScope.SESSION)).transform(dateStr -> {
			return ETGDBDateUtil.parseFromDB((String)dateStr);
		});
		final Optional<DateTime> baselineDate = Optional.fromNullable(message.getProperty(SessionVars.BASELINE_DATE, PropertyScope.SESSION)).transform(dateStr -> {
			return ETGDBDateUtil.parseFromDB((String)dateStr);
		});
		List<PTTripItem> tripList = sd.getEstimatedTimetableDelivery().get(0)
		.getEstimatedJourneyVersionFrame().get(0).getEstimatedVehicleJourney()
		.stream().map(vehicleJourney -> {
			PTTripItem tripItem = new PTTripItem();
			tripItem.setTrip(vehicleJourney.getDatedVehicleJourneyRef().getValue());
			final String direction = (vehicleJourney.getDirectionRef().getValue());
			tripItem.setIsUp(TripDirection.UP.name().equalsIgnoreCase(direction));
			TrainServiceInformation serviceInfo = (TrainServiceInformation)((JAXBElement)vehicleJourney.getExtensions().getAny().get(0)).getValue();
			tripItem.setTrainType(serviceInfo.getTrainType());
			tripItem.setSpeedBandName(serviceInfo.getSpeedBandName());
			tripItem.setTripDate(ETGDateConvertUtil.convertXmlGregorianToDate(serviceInfo.getDate()));
			tripItem.setCalls(vehicleJourney.getEstimatedCalls().getEstimatedCall().stream().map(call -> {
				PTCallItem callItem = new PTCallItem();
				callItem.setS(call.getStopPointRef().getValue());
				if (call.getAimedArrivalTime() != null ) {	
					DateTime aimedArrivalTime = ETGDBDateUtil.convertFromXml(call.getAimedArrivalTime());
					if(targetDate.isPresent()) {	
						Integer diffDays = Days.daysBetween(new LocalDate(aimedArrivalTime), new LocalDate(baselineDate.get())).getDays();
						callItem.setA(aimedArrivalTime
								.withYear(targetDate.get().getYear())
								.withMonthOfYear(targetDate.get().getMonthOfYear())
								.withDayOfMonth(targetDate.get().getDayOfMonth())
								.minusDays(diffDays));
					} else {
						callItem.setA(aimedArrivalTime);
					}

				}
				if (call.getAimedDepartureTime() != null) {
					DateTime aimedDepartureTime = ETGDBDateUtil.convertFromXml(call.getAimedDepartureTime());
					if(targetDate.isPresent()) {
						Integer diffDays = Days.daysBetween(new LocalDate(aimedDepartureTime), new LocalDate(baselineDate.get())).getDays();
						callItem.setD(aimedDepartureTime
								.withYear(targetDate.get().getYear())
								.withMonthOfYear(targetDate.get().getMonthOfYear())
								.withDayOfMonth(targetDate.get().getDayOfMonth())
								.minusDays(diffDays));
					} else {
						callItem.setD(aimedDepartureTime);
					}
					
					
				}
				return callItem;
			}).collect(Collectors.toList()));
			return tripItem;
		}).collect(Collectors.toList());
		message.setPayload(tripList);
		return message;
	}

}