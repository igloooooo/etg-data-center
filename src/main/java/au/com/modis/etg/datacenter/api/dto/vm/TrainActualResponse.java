package au.com.modis.etg.datacenter.api.dto.vm;

import java.util.List;

public class TrainActualResponse {
	public List<TrainActualDto> getVm() {
		return vm;
	}

	public void setVm(List<TrainActualDto> vm) {
		this.vm = vm;
	}

	private List<TrainActualDto> vm;
	
	
}
