package au.com.modis.etg.datacenter.email;

import java.util.HashMap;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;

public class PTGEmailVars extends AbstractMessageTransformer{
	private static final HashMap<String, String> BorderNameMap = new HashMap<String, String>() {{
	    put("1",    "Freight");
	    put("2", "Illawarra");
	    put("3",   "Main A");
	    put("4",   "Main B");
	    put("5",   "North");
	    put("6",   "Outer");
	    put("7",   "South Coast");
	    put("8",   "West");
	}};
	private static DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		String fileName = message.getProperty("originalFilename", PropertyScope.INBOUND);
		String borderName = getBorderName(fileName);
		Integer incrementDay = getIncrementDay(fileName);
		String date = fmt.print(DateTime.now().minusDays(-1*incrementDay));
		message.setProperty("dataset", date, PropertyScope.OUTBOUND);
		message.setProperty("borderName", borderName, PropertyScope.OUTBOUND);
		message.setProperty("incrementDay", getIncrementDay(fileName), PropertyScope.OUTBOUND);
		return message;
	}
	
	/**
	 * the file name format is 6.0.svg
	 * 6: board id
	 * 0: increment day
	 * @param filename
	 * @return
	 */
	private String getBorderName(String filename) {
		String index = filename.substring(0, filename.indexOf("."));
		if(BorderNameMap.containsKey(index)) {
			return BorderNameMap.get(index);
		} else {
			return "UNKNOWN";
		}
	}
	
	private Integer getIncrementDay(String filename) {
		String subStr = filename.substring(filename.indexOf(".")+1);
		Integer incrementDay = Integer.valueOf(subStr.substring(0, subStr.indexOf(".")));
		if(incrementDay < 0) {
			logger.error("file name [" + filename + "] is not right format");
		}
		return incrementDay;
	}

}
