package au.com.modis.etg.datacenter.transformer.et;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import au.com.modis.etg.db.dto.DBDtoBase;

/**
 * the namespace of SIRI ET has problem, which is about [xmlns:p="http://www.siri.org.uk/siri"]
 * it has to be changed to [xmlns:n0="http://www.siri.org.uk/siri"]
 * @author nicholaszhu
 *
 */
public class SIRIETNamespaceHacker extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		String rawXML = (String) message.getPayload();
		return message;
	}

}
