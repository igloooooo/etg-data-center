package au.com.modis.etg.datacenter.transformer.pt;

import java.io.StringReader;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.siri.siri.DatedVehicleJourneyStructure;
import uk.org.siri.siri.ServiceDelivery;

public class PTRawXMLFilter extends AbstractMessageTransformer{
	private static final Logger LOGGER = LoggerFactory.getLogger(PTRawXMLFilter.class);
	public JAXBContext getJaxbContext() {
		return jaxbContext;
	}

	public void setJaxbContext(JAXBContext jaxbContext) {
		this.jaxbContext = jaxbContext;
	}
	private JAXBContext jaxbContext;
	
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		String rawJMS = (String)message.getPayload();
		String trip = (String) message.getProperty("trip", PropertyScope.SESSION);
		if (isIncludeTrip(trip, rawJMS)) {
			return message;
		} else {
			return null;
		}
	}
	
	private Boolean isIncludeTrip(String trip, String rawJMS) {
		try {
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(rawJMS);
			ServiceDelivery sd = (ServiceDelivery) unmarshaller.unmarshal(reader);
			List<DatedVehicleJourneyStructure> svjsList = sd.getProductionTimetableDelivery().get(0).getDatedTimetableVersionFrame().get(0).getDatedVehicleJourney();
			for(DatedVehicleJourneyStructure svjs : svjsList) {
				if(trip.equalsIgnoreCase(svjs.getDatedVehicleJourneyCode())) {
					LOGGER.info("find trip" + trip);
					return true;
				}
			}
			return false;		
		} catch (JAXBException e) {
			return false;
		}
	}
}
