package au.com.modis.etg.datacenter.transformer;

import java.io.IOException;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import au.com.modis.etg.common.utils.ZipUtil;
import au.com.modis.etg.db.dto.DBDtoBase;

public class DBDtoToXml extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		DBDtoBase resp = (DBDtoBase) message.getPayload();
		try {
			message.setPayload(ZipUtil.uncompressString(resp.getZip()));
		} catch (IOException e) {
			logger.error(e);
		}
		return message;
	}

}
