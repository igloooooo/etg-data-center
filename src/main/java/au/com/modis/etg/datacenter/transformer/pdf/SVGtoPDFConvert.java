package au.com.modis.etg.datacenter.transformer.pdf;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;

import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.fop.svg.PDFTranscoder;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

public class SVGtoPDFConvert extends AbstractMessageTransformer {

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		try {
			String svg = message.getPayloadAsString();
			svg = svg.replaceAll("rgba\\(0,0,0,0\\)", "none");
			svg = svg.replaceAll("font-size: 8px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font-size: 8px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"6\">");
			svg = svg.replaceAll("font-size: 8px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"0\">", "font-size: 8px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"6\">");
			svg = svg.replaceAll("font-size: 10px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font-size: 10px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"8\">");
			svg = svg.replaceAll("font-size: 10px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"0\">", "font-size: 10px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"8\">");
			svg = svg.replaceAll("font-size: 12px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font-size: 12px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"10\">");
			svg = svg.replaceAll("font-size: 12px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"0\">", "font-size: 12px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"10\">");
			svg = svg.replaceAll("font: 8px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\"", "font: 8px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"6\"");
			svg = svg.replaceAll("font: 8px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"0\"", "font: 8px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"6\"");
			svg = svg.replaceAll("font: 10px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\"", "font: 10px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"8\"");
			svg = svg.replaceAll("font: 10px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"0\"", "font: 10px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"8\"");
			svg = svg.replaceAll("font: 12px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"0\">", "font: 12px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"10\">");
			svg = svg.replaceAll("font: 12px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font: 12px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"10\">");
			
			svg = svg.replaceAll("font-size: 16px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font-size: 16px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"12\">");
			svg = svg.replaceAll("font-size: 16px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"0\">", "font-size: 16px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"12\">");
			svg = svg.replaceAll("font-size: 20px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font-size: 20px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"12\">");
			svg = svg.replaceAll("font-size: 24px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font-size: 24px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"14\">");
			svg = svg.replaceAll("font-size: 26px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font-size: 26px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"16\">");
			svg = svg.replaceAll("font: bold 26px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font: bold 26px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"18\">");
			svg = svg.replaceAll("font: bold 24px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font: bold 24px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"16\">");
			svg = svg.replaceAll("font: bold 20px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font: bold 20px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"12\">");
			svg = svg.replaceAll("font: 16px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font: 16px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"12\">");
			svg = svg.replaceAll("font: 16px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"0\">", "font: 16px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"12\">");
			
			svg = svg.replaceAll("</tspan><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"10\">", "</tspan><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"18\">");
			svg = svg.replaceAll("</tspan><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"16\">", "</tspan><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"28\">");
			svg = svg.replaceAll("</tspan><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"12\">", "</tspan><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"22\">");
			
			svg = svg.replaceAll("font-size: 14px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">REMARKS", "font-size: 14px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"11\">REMARKS");
			svg = svg.replaceAll("font: bold 14px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">REMARKS", "font: bold 14px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"11\">REMARKS");
			svg = svg.replaceAll("font: bold 10px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font: bold 10px sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"8\">");
			svg = svg.replaceAll("font-size: 24px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"start\" x=\"0\" y=\"0\">", "font-size: 24px; line-height: normal; font-family: sans-serif;\"><tspan alignment-baseline=\"hanging\" text-anchor=\"end\" x=\"0\" y=\"14\">");
			svg = svg.replaceAll("rgba\\((\\s*[ 0-9]+,\\s*[ 0-9]+,\\s*[ 0-9]+),(\\s*[ 0-9.]+)\\)", "rgb($1)");
			svg = svg.replaceAll("stroke-dasharray=\"\"", "stroke-dasharray=\"none\"");
            InputStream inputStream = new ByteArrayInputStream(svg.getBytes());
            TranscoderInput transcoderInput = new TranscoderInput(inputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            TranscoderOutput transcoderOutput = new TranscoderOutput(byteArrayOutputStream);
            PDFTranscoder pdfTranscoder = new PDFTranscoder();
            pdfTranscoder.addTranscodingHint(PDFTranscoder.KEY_XML_PARSER_CLASSNAME, "org.apache.xerces.parsers.SAXParser");
            pdfTranscoder.transcode(transcoderInput, transcoderOutput);

            byte[] pdf = byteArrayOutputStream.toByteArray();
            message.setPayload(pdf);
        } catch (TranscoderException e) {
            e.printStackTrace();
        } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return message;
	}

}
