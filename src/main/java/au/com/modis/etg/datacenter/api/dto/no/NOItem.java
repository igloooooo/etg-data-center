package au.com.modis.etg.datacenter.api.dto.no;

import java.io.Serializable;

import org.joda.time.DateTime;

public class NOItem implements Serializable{
	public DateTime getRequestedStartDateTime() {
		return requestedStartDateTime;
	}
	public void setRequestedStartDateTime(DateTime requestedStartDateTime) {
		this.requestedStartDateTime = requestedStartDateTime;
	}
	public DateTime getRequestedEndDateTime() {
		return requestedEndDateTime;
	}
	public void setRequestedEndDateTime(DateTime requestedEndDateTime) {
		this.requestedEndDateTime = requestedEndDateTime;
	}
	public DateTime getApprovedStartDateTime() {
		return approvedStartDateTime;
	}
	public void setApprovedStartDateTime(DateTime approvedStartDateTime) {
		this.approvedStartDateTime = approvedStartDateTime;
	}
	public DateTime getApprovedEndDateTime() {
		return approvedEndDateTime;
	}
	public void setApprovedEndDateTime(DateTime approvedEndDateTime) {
		this.approvedEndDateTime = approvedEndDateTime;
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 5665120349829834944L;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getFromNode() {
		return fromNode;
	}
	public void setFromNode(String fromNode) {
		this.fromNode = fromNode;
	}
	public String getToNode() {
		return toNode;
	}
	public void setToNode(String toNode) {
		this.toNode = toNode;
	}
	private DateTime requestedStartDateTime;
	private DateTime requestedEndDateTime;
	private DateTime approvedStartDateTime;
	private DateTime approvedEndDateTime;
	private String id;
	private String name;
	private String comments;
	private String fromNode;
	private String toNode;
	
	
}
