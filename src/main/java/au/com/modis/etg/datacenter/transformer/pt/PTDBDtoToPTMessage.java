package au.com.modis.etg.datacenter.transformer.pt;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;

import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.transformer.OutboundProperty;
import au.com.modis.etg.datacenter.transformer.SessionVars;
import au.com.modis.etg.datacenter.util.ETGDateConvertUtil;
import uk.org.siri.siri.DatedTimetableVersionFrameStructure;
import uk.org.siri.siri.MessageRefStructure;
import uk.org.siri.siri.ProductionTimetableDeliveryStructure;
import uk.org.siri.siri.ServiceDelivery;

public class PTDBDtoToPTMessage extends AbstractMessageTransformer {

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		String rawTargetDate = message.getProperty(SessionVars.TARGET_DATE, PropertyScope.SESSION);
		if (StringUtils.isEmpty(rawTargetDate)) {
			return message;
		}
		
		ServiceDelivery sd = ((ServiceDelivery) message.getPayload());
		final DateTime targetDate = ETGDBDateUtil.parseFromDB(rawTargetDate);
		final String requestRef = message.getProperty(SessionVars.REQUEST_REF, PropertyScope.SESSION);
		if (requestRef != null) {
			MessageRefStructure messageRef = new MessageRefStructure();
			messageRef.setValue(requestRef);
			sd.setRequestMessageRef(messageRef);
			// this will set JMS header
			message.setOutboundProperty(OutboundProperty.CORELATIONID, requestRef);
			message.setOutboundProperty(OutboundProperty.MESSAGEID, sd.getResponseMessageIdentifier().getValue());
		}
		
		sd.setResponseTimestamp(ETGDBDateUtil.convertToTargetDate(sd.getResponseTimestamp(), targetDate));

		ProductionTimetableDeliveryStructure productionTimetableDelivery = sd.getProductionTimetableDelivery().get(0);
		productionTimetableDelivery.setResponseTimestamp(
				ETGDBDateUtil.convertToTargetDate(productionTimetableDelivery.getResponseTimestamp(), targetDate));

		DatedTimetableVersionFrameStructure datedTimetableVersionFrame = productionTimetableDelivery
				.getDatedTimetableVersionFrame().get(0);
		datedTimetableVersionFrame.setRecordedAtTime(
				ETGDBDateUtil.convertToTargetDate(datedTimetableVersionFrame.getRecordedAtTime(), targetDate));

		datedTimetableVersionFrame.getDatedVehicleJourney().stream().forEach(datedVehicleJourney -> {
			final DateTime baselineDate = ETGDBDateUtil
					.parseFromXMLDate(datedVehicleJourney.getFramedVehicleJourneyRef().getDataFrameRef().getValue());
			datedVehicleJourney.getFramedVehicleJourneyRef().getDataFrameRef().setValue(
					targetDate.getYear() + "-" + targetDate.getMonthOfYear() + "-" + targetDate.getDayOfMonth());
			datedVehicleJourney.getDatedCalls().getDatedCall().stream().forEach(call -> {
				if (call.getAimedArrivalTime() != null) {
					call.setAimedArrivalTime(ETGDBDateUtil.convertToTargetDate(call.getAimedArrivalTime(), targetDate,
							ETGDateConvertUtil.differentWithBaseline(baselineDate, call.getAimedArrivalTime())));
				}
				if (call.getAimedDepartureTime() != null) {
					call.setAimedDepartureTime(ETGDBDateUtil.convertToTargetDate(call.getAimedDepartureTime(),
							targetDate, ETGDateConvertUtil.differentWithBaseline(baselineDate, call.getAimedDepartureTime())));
				}

			});

		});
		return message;
	}



}
