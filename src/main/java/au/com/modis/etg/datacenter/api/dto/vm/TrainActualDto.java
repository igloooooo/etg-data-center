package au.com.modis.etg.datacenter.api.dto.vm;

import org.joda.time.DateTime;

public class TrainActualDto {
	public String getTrip() {
		return trip;
	}
	public void setTrip(String trip) {
		this.trip = trip;
	}
	public DateTime getTime() {
		return time;
	}
	public void setTime(DateTime time) {
		this.time = time;
	}
	public Integer getActualType() {
		return actualType;
	}
	public void setActualType(Integer type) {
		this.actualType = type;
	}
	public String getStopRef() {
		return stopRef;
	}
	public void setStopRef(String stopRef) {
		this.stopRef = stopRef;
	}
	private String trip;
	private DateTime time;
	/**
	 * 0 - arrival 1 - departure
	 */
	private Integer actualType;
	private String stopRef;
	
	
}
