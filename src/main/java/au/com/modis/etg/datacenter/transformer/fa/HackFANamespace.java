package au.com.modis.etg.datacenter.transformer.fa;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

public class HackFANamespace extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		String xml = (String)message.getPayload();
		xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><ns2:fleetAllocationResponseMsg xmlns:ns2=\"http://www.quintiq.com/ServicePlanner/FleetAllocResp\">"
				+ xml.substring(458).replaceAll("ns7:fleetAllocationResponseMsg", "ns2:fleetAllocationResponseMsg");
		message.setPayload(xml);
		return message;
	}

}
