package au.com.modis.etg.datacenter.constants;

public enum TripDirection {
	UP, DOWN
}
