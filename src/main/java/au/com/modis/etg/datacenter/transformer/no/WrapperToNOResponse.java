package au.com.modis.etg.datacenter.transformer.no;

import java.util.List;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import au.com.modis.etg.datacenter.api.dto.no.NOItem;
import au.com.modis.etg.datacenter.api.dto.no.NetworkOutageResponse;

public class WrapperToNOResponse extends AbstractMessageTransformer{

	@SuppressWarnings("unchecked")
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		List<NOItem> list = (List<NOItem>)message.getPayload();
		NetworkOutageResponse response = new NetworkOutageResponse();
		response.setNoItems(list);
		message.setPayload(response);
		return message;
	}

}
