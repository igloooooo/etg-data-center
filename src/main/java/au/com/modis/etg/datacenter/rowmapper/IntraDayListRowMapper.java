package au.com.modis.etg.datacenter.rowmapper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.joda.time.DateTime;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.api.dto.IntraDayListDto;

public class IntraDayListRowMapper implements Callable {
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		IntraDayListDto sb = new IntraDayListDto();
		Map map = (Map) eventContext.getMessage().getPayload();
		for (Object name : map.keySet()) {
			Object value = map.get(name);
			if (name.toString().equalsIgnoreCase(RowColumnConstants.MESSAGE_ID)) {
				sb.setId((String) value);
			} else if (name.toString().equalsIgnoreCase(RowColumnConstants.MESSAGE_TS)) {
				sb.setTimestamp(ETGDBDateUtil.formatToDateTime(new DateTime((Date) value)));
			}
		}
		return sb;
	}

}
