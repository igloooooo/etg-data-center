package au.com.modis.etg.datacenter.api.dto;

import java.io.Serializable;

public class DatasetListDto implements Serializable{

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = -2001661467342813577L;
	private String name;
	private Integer count;
	private String messageType;
	
	

}
