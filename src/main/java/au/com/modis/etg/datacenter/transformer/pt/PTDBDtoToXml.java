package au.com.modis.etg.datacenter.transformer.pt;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;

import au.com.modis.etg.datacenter.rowmapper.RowColumnConstants;
import au.com.modis.etg.common.utils.ZipUtil;
import au.com.modis.etg.db.dto.DBDtoBase;
import au.com.modis.etg.db.dto.PTDBDto;
import uk.org.siri.siri.Siri;

public class PTDBDtoToXml extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		PTDBDto dto = (PTDBDto) message.getPayload();
		try {
			message.setPayload(ZipUtil.uncompressString(dto.getZip()));
			message.setOutboundProperty(RowColumnConstants.PART_NO, dto.getPartNO());
			message.setOutboundProperty(RowColumnConstants.TOTAL_PARTS, dto.getTotalParts());
		} catch (IOException e) {
			logger.error(e);
		}
		return message;
	}
	
	

}
