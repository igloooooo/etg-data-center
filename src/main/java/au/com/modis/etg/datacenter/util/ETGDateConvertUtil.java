package au.com.modis.etg.datacenter.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;

import au.com.modis.etg.common.utils.ETGDBDateUtil;

public final class ETGDateConvertUtil {
	public static Duration differentWithBaseline(DateTime baselineDate, XMLGregorianCalendar source) {
		DateTime raw = ETGDBDateUtil.convertFromXml(source);
		try {
			if (baselineDate.getDayOfMonth() != raw.getDayOfMonth()) {
				// in this case, the train runs cross the day, so we need add one day
				return DatatypeFactory.newInstance().newDuration(24 * 60 * 60 * 1000);
			} else {
				return DatatypeFactory.newInstance().newDuration(0L);
			}
		} catch (DatatypeConfigurationException e) {
			return null;
		}
	}
	
	public static Duration differentDayWithBaseline(DateTime baselineDate, DateTime targetDate) {
		Integer diffDays = Days.daysBetween(new LocalDate(targetDate), new LocalDate(baselineDate)).getDays();
		try {
			return DatatypeFactory.newInstance().newDuration(diffDays * 24 * 60 * 60 * 1000);
		} catch (DatatypeConfigurationException e) {
			return null;
		}
	}
	
	public static XMLGregorianCalendar getXMLGregorianCalendarNow() 
            throws DatatypeConfigurationException {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
        XMLGregorianCalendar now = 
            datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
        return now;
    }
	
	public static XMLGregorianCalendar getXMLGregorianCalendarStartOfDay(GregorianCalendar gc) 
            throws DatatypeConfigurationException {
        DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
        gc.set(GregorianCalendar.HOUR_OF_DAY,0);
        gc.set(GregorianCalendar.MINUTE,0);
        gc.set(GregorianCalendar.SECOND,0);
        gc.set(GregorianCalendar.MILLISECOND,0);
        XMLGregorianCalendar now = 
            datatypeFactory.newXMLGregorianCalendar(gc);
        return now;
    }
	
	public static String convertXmlGregorianToString(XMLGregorianCalendar xc)
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
 
        GregorianCalendar gCalendar = xc.toGregorianCalendar();
 
        //Converted to date object
        Date date = gCalendar.getTime();
 
        //Formatted to String value
        String dateString = df.format(date);
 
        return dateString;
    }
	
	public static String convertXmlGregorianToDate(XMLGregorianCalendar xc)
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
 
        GregorianCalendar gCalendar = xc.toGregorianCalendar();
 
        //Converted to date object
        Date date = gCalendar.getTime();
 
        //Formatted to String value
        String dateString = df.format(date);
 
        return dateString;
    }
}
