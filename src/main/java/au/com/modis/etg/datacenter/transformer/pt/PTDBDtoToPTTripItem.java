package au.com.modis.etg.datacenter.transformer.pt;

import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBElement;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;
import org.python.google.common.base.Optional;

import com.quintiq.serviceplanner.siri.TrainServiceInformation;

import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.api.dto.pt.PTCallItem;
import au.com.modis.etg.datacenter.api.dto.pt.PTTripItem;
import au.com.modis.etg.datacenter.transformer.SessionVars;
import uk.org.siri.siri.ServiceDelivery;

/**
 * this response is for visualize of playback: generate PTTripItem
 * @author nicholaszhu
 *
 */
public class PTDBDtoToPTTripItem extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		ServiceDelivery sd = ((ServiceDelivery) message.getPayload());
		final Optional<DateTime> targetDate = Optional.fromNullable(message.getProperty(SessionVars.TARGET_DATE, PropertyScope.SESSION)).transform(dateStr -> {
			return ETGDBDateUtil.parseFromDB((String)dateStr);
		});
		final Optional<DateTime> baselineDate = Optional.fromNullable(message.getProperty(SessionVars.BASELINE_DATE, PropertyScope.SESSION)).transform(dateStr -> {
			return ETGDBDateUtil.parseFromDB((String)dateStr);
		});
		List<PTTripItem> tripList = sd.getProductionTimetableDelivery().get(0)
		.getDatedTimetableVersionFrame().get(0).getDatedVehicleJourney()
		.stream().map(vehicleJourney -> {
			PTTripItem tripItem = new PTTripItem();
			tripItem.setTrip(vehicleJourney.getFramedVehicleJourneyRef().getDatedVehicleJourneyRef());
			tripItem.setTripDate(vehicleJourney.getFramedVehicleJourneyRef().getDataFrameRef().getValue());
			tripItem.setIsUp(false);
			TrainServiceInformation serviceInfo = (TrainServiceInformation)((JAXBElement)vehicleJourney.getExtensions().getAny().get(0)).getValue();
			tripItem.setTrainType(serviceInfo.getTrainType());
			tripItem.setSpeedBandName(serviceInfo.getSpeedBandName());
			tripItem.setCalls(vehicleJourney.getDatedCalls().getDatedCall().stream().map(call -> {
				PTCallItem callItem = new PTCallItem();
				callItem.setS(call.getStopPointRef().getValue());
				if (call.getAimedArrivalTime() != null ) {	
					DateTime aimedArrivalTime = ETGDBDateUtil.convertFromXml(call.getAimedArrivalTime());
					if(targetDate.isPresent()) {	
						Integer diffDays = Days.daysBetween(new LocalDate(aimedArrivalTime), new LocalDate(baselineDate.get())).getDays();
						callItem.setA(aimedArrivalTime
								.withYear(targetDate.get().getYear())
								.withMonthOfYear(targetDate.get().getMonthOfYear())
								.withDayOfMonth(targetDate.get().getDayOfMonth())
								.minusDays(diffDays));
					} else {
						callItem.setA(aimedArrivalTime);
					}

				}
				if (call.getAimedDepartureTime() != null) {
					DateTime aimedDepartureTime = ETGDBDateUtil.convertFromXml(call.getAimedDepartureTime());
					if(targetDate.isPresent()) {
						Integer diffDays = Days.daysBetween(new LocalDate(aimedDepartureTime), new LocalDate(baselineDate.get())).getDays();
						callItem.setD(aimedDepartureTime
								.withYear(targetDate.get().getYear())
								.withMonthOfYear(targetDate.get().getMonthOfYear())
								.withDayOfMonth(targetDate.get().getDayOfMonth())
								.minusDays(diffDays));
					} else {
						callItem.setD(aimedDepartureTime);
					}
					
					
				}
				return callItem;
			}).collect(Collectors.toList()));
			return tripItem;
		}).collect(Collectors.toList());
		message.setPayload(tripList);
		return message;
	}

}
