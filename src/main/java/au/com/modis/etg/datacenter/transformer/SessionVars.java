package au.com.modis.etg.datacenter.transformer;

public interface SessionVars {
	String REQUEST_REF = "requestRef";
	String REQUEST_DATE = "requestDate";
	String TARGET_DATE = "targetDate";
	String BASELINE_DATE = "baselineDate";
}
