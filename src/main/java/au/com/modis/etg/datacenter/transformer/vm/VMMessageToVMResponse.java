package au.com.modis.etg.datacenter.transformer.vm;

import java.util.List;
import java.util.stream.Collectors;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;
import org.mule.transport.NullPayload;

import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.api.dto.vm.TrainActualDto;
import au.com.modis.etg.datacenter.api.dto.vm.TrainActualResponse;
import uk.org.siri.siri.Siri;
import uk.org.siri.siri.VehicleActivityStructure.MonitoredVehicleJourney;
import uk.org.siri.siri.VehicleMonitoringDeliveryStructure;

/**
 * this response is for visualize of playback
 * @author nicholaszhu
 *
 */
public class VMMessageToVMResponse extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		TrainActualResponse response = new TrainActualResponse();
		if( message.getPayload() instanceof NullPayload) {
			// no result?
		} else {
			List<Siri> messageList = (List<Siri>)message.getPayload();
			response.setVm(messageList.stream().map(siri -> {
				MonitoredVehicleJourney monitoredVehicleJourney = siri.getServiceDelivery()
						.getVehicleMonitoringDelivery().get(0)
						.getVehicleActivity().get(0)
						.getMonitoredVehicleJourney();
				TrainActualDto dto = new TrainActualDto();
				String trip = monitoredVehicleJourney.getFramedVehicleJourneyRef().getDatedVehicleJourneyRef();
				dto.setTrip(trip);
				dto.setStopRef(monitoredVehicleJourney.getMonitoredCall().getStopPointRef().getValue());
				if (monitoredVehicleJourney.getMonitoredCall().getExpectedArrivalTime() != null) {
					dto.setActualType(0);
					dto.setTime(ETGDBDateUtil.convertFromXml(monitoredVehicleJourney.getMonitoredCall().getExpectedArrivalTime()));
				} else if (monitoredVehicleJourney.getMonitoredCall().getActualDepartureTime() != null) {
					dto.setActualType(1);
					dto.setTime(ETGDBDateUtil.convertFromXml(monitoredVehicleJourney.getMonitoredCall().getActualDepartureTime()));
				}
				return dto;
			}).collect(Collectors.toList()));
			
		}
		message.setPayload(response);
		return message;
	}

}
