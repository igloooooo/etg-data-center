package au.com.modis.etg.datacenter.transformer.pt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import au.com.modis.etg.datacenter.api.dto.pt.PTTripItem;
import au.com.modis.etg.datacenter.api.dto.pt.TimeTableResponse;

/**
 * this response is for visualize of playback
 * @author nicholaszhu
 *
 */
public class WrapperToPTResponse extends AbstractMessageTransformer{

	@SuppressWarnings("unchecked")
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		List<PTTripItem> itemList = (List<PTTripItem>)message.getPayload();
		// we need remove the duplicated trip, especially for TRIMS
		// latest or the last one will be treated as correct one
		Map<String, PTTripItem> pool = new HashMap<String, PTTripItem>();
		itemList.stream().forEach(item -> {
			String key = item.getTrip()+ "-" + item.getTripDate();
			if(pool.containsKey(key)) {
				logger.warn("Find duplicated trip: " + key);
			}
			pool.put(key, item);
		});
		
		TimeTableResponse response = new TimeTableResponse();
		response.setTrips(new ArrayList<PTTripItem>(pool.values()));
		return response;
	}

}
