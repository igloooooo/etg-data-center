package au.com.modis.etg.datacenter.rowmapper;

import java.util.Date;
import java.util.Map;

import org.joda.time.DateTime;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.db.dto.IMDBDto;
import oracle.sql.CLOB;

@SuppressWarnings("deprecation")
public class IMRowMapper implements Callable{
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		IMDBDto sb = new IMDBDto();
		Map map = (Map) eventContext.getMessage().getPayload();
		for (Object name : map.keySet()) {
			Object value = map.get(name);
			if (name.toString().equalsIgnoreCase(RowColumnConstants.MESSAGE_TS)) {
				sb.setTriggerTime(ETGDBDateUtil.formatToDateTime(new DateTime((Date) value)));
			} else if (name.toString().equalsIgnoreCase(RowColumnConstants.SIRI_SX_MESSAGE_ID)) {
				sb.setId((String) value);
			} else if (name.toString().equalsIgnoreCase(RowColumnConstants.MESSAGE_PAYLOAD)) {
				if (value instanceof CLOB) {
					sb.setZip(org.apache.commons.io.IOUtils.toString(((CLOB) value).getCharacterStream()));
				}
			}
		}
		return sb;
	}
}
