package au.com.modis.etg.datacenter.transformer.im;

import org.joda.time.DateTime;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;

import com.quintiq.serviceplanner.possessionsresp.PossessionsResponseMsg;

import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.transformer.SessionVars;

public class IMDBDtoToIMMessage extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		final DateTime targetDate = ETGDBDateUtil.parseFromDB(message.getProperty(SessionVars.TARGET_DATE, PropertyScope.SESSION));
		PossessionsResponseMsg msg = ((PossessionsResponseMsg) message.getPayload());
		DateTime originalTime = DateTime.parse(msg.getHeader().getTimeStamp());
		msg.getHeader().setTimeStamp(
				originalTime.withDate(targetDate.getYear(), targetDate.getDayOfMonth(), targetDate.getDayOfMonth()).toString());
		
		message.setPayload(msg);
		return message;
	}

}
