package au.com.modis.etg.datacenter.transformer.no;

import javax.xml.datatype.Duration;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;

import com.quintiq.serviceplanner.possessionsresp.PossessionsResponseMsg;

import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.transformer.OutboundProperty;
import au.com.modis.etg.datacenter.transformer.SessionVars;
import au.com.modis.etg.datacenter.util.ETGDateConvertUtil;

public class NODBDtoToNOMessage extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		String rawTargetDate = message.getProperty(SessionVars.TARGET_DATE, PropertyScope.SESSION);
		if (StringUtils.isEmpty(rawTargetDate)) {
			return message;
		}
		final DateTime targetDate = ETGDBDateUtil.parseFromDB(rawTargetDate);
		final DateTime baselineDate = ETGDBDateUtil.parseFromDB(message.getProperty(SessionVars.BASELINE_DATE, PropertyScope.SESSION));
		final String requestRef = message.getProperty(SessionVars.REQUEST_REF, PropertyScope.SESSION);
		PossessionsResponseMsg msg = ((PossessionsResponseMsg) message.getPayload());
		DateTime originalTime = DateTime.parse(msg.getHeader().getTimeStamp());
		msg.getHeader().setTimeStamp(
				originalTime.withDate(targetDate.getYear(), targetDate.getDayOfMonth(), targetDate.getDayOfMonth()).toString());
		
		// set request ref
		if (requestRef != null) {
			msg.getHeader().setCorrelationID(requestRef);
			message.setOutboundProperty(OutboundProperty.CORELATIONID, requestRef);
			message.setOutboundProperty(OutboundProperty.MESSAGEID, msg.getHeader().getMsgID());
		}
		msg.getBody().getPossessionsResponse().getOutages().getOutage().forEach(outage -> {
			Duration diff = ETGDateConvertUtil.differentDayWithBaseline(baselineDate, targetDate);
			outage.setApprovedEndDateTime(ETGDBDateUtil.convertToTargetDate(outage.getApprovedEndDateTime(), targetDate, diff));
			outage.setApprovedStartDateTime(ETGDBDateUtil.convertToTargetDate(outage.getApprovedStartDateTime(), targetDate, diff));
			outage.setRequestedEndDateTime(ETGDBDateUtil.convertToTargetDate(outage.getRequestedEndDateTime(), targetDate, diff));
			outage.setRequestedStartDateTime(ETGDBDateUtil.convertToTargetDate(outage.getRequestedStartDateTime(), targetDate, diff));
		});
		message.setPayload(msg);
		return message;
	}

}
