package au.com.modis.etg.datacenter.api.dto;

import java.io.Serializable;

public class IntraDayListDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8679341454027629857L;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	private String id;
	private String timestamp;
	
	
}
