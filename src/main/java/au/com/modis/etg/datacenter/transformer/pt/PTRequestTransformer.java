package au.com.modis.etg.datacenter.transformer.pt;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;

import au.com.modis.etg.callback.dto.PTRequestDto;
import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.transformer.SessionVars;
import uk.org.siri.siri.ServiceRequest;

public class PTRequestTransformer extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		// TODO Auto-generated method stub
		ServiceRequest serviceRequest = (ServiceRequest)message.getPayload();
		
		// generate pt request
		PTRequestDto ptReq = new PTRequestDto();
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
		String requestDate = fmt.print(ETGDBDateUtil
				.convertFromXml(serviceRequest.getRequestTimestamp()));
		ptReq.setRequestDate(requestDate);
		// send request id as session varibale
		message.setProperty(SessionVars.REQUEST_REF, serviceRequest.getMessageIdentifier(), PropertyScope.SESSION);
				
		message.setPayload(ptReq);
		return message;
	}

}
