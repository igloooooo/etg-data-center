package au.com.modis.etg.datacenter.rowmapper;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import org.joda.time.DateTime;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.api.dto.statistic.VMStatisDto;
import au.com.modis.etg.db.dto.FADBDto;
import oracle.sql.CLOB;

public class VMStatisticRowMapper implements Callable{
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		VMStatisDto sb = new VMStatisDto();
		Map map = (Map) eventContext.getMessage().getPayload();
		for (Object name : map.keySet()) {
			Object value = map.get(name);
			if (name.toString().equalsIgnoreCase(RowColumnConstants.MESSAGE_INV)) {
				sb.setInterval(new DateTime(((Timestamp) value).getTime()));
			} else if (name.toString().equalsIgnoreCase(RowColumnConstants.MESSAGE_COUNT)) {
				sb.setCount(((BigDecimal) value).intValue());
			} 
		}
		return sb;
	}
}
