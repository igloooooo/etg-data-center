package au.com.modis.etg.datacenter.api.dto.statistic;

import org.joda.time.DateTime;

public class VMStatisDto {
	public DateTime getInterval() {
		return interval;
	}
	public void setInterval(DateTime interval) {
		this.interval = interval;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	private DateTime interval;
	private Integer count;
	
	
}
