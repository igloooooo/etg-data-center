package au.com.modis.etg.datacenter.transformer.vm;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.api.dto.statistic.VMStatisDto;

public class VMStatisticConvertTargetDate extends AbstractMessageTransformer {

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		Map<String, String> queryParams = message.getInboundProperty("http.query.params");
		DateTime targetDate = Optional.ofNullable(queryParams.get("targetDate"))
				.map(str -> ETGDBDateUtil.parseFromDB(str)).orElse(DateTime.now());
		@SuppressWarnings("unchecked")
		List<VMStatisDto> statisDtoList = (List<VMStatisDto>) message.getPayload();
		message.setPayload(statisDtoList.stream().map(dto -> {
			dto.setInterval(dto.getInterval()
					.withDayOfMonth(targetDate.getDayOfMonth())
					.withMonthOfYear(targetDate.getMonthOfYear())
					.withYear(targetDate.getYear()));
			return dto;
		}).collect(Collectors.toList()));
		return message;
	}

}
