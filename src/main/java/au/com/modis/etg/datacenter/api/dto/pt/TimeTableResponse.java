package au.com.modis.etg.datacenter.api.dto.pt;

import java.io.Serializable;
import java.util.List;

public class TimeTableResponse implements Serializable {
	public List<PTTripItem> getTrips() {
		return trips;
	}

	public void setTrips(List<PTTripItem> trips) {
		this.trips = trips;
	}

	private List<PTTripItem> trips;
	
}
