package au.com.modis.etg.datacenter.transformer.fa;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;

import com.quintiq.serviceplanner.fleetallocresp.FleetAllocationRequestMsg;

import au.com.modis.etg.callback.dto.FARequestDto;
import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.transformer.SessionVars;

public class FARequestTransformer extends AbstractMessageTransformer {

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		FleetAllocationRequestMsg msg = (FleetAllocationRequestMsg) message.getPayload();
		FARequestDto faReq = new FARequestDto();
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
		faReq.setRequestDate(fmt.print(ETGDBDateUtil
				.convertFromXml(msg.getBody().getFleetAllocRequest().getFleetAllocDateRange().getStartDateTime())));
		message.setPayload(faReq);
		// set request_ref as session var
		message.setProperty(SessionVars.REQUEST_REF, msg.getHeader().getMsgID(), PropertyScope.SESSION);
		
		return message;
	}

}
