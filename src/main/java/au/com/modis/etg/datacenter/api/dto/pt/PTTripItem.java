package au.com.modis.etg.datacenter.api.dto.pt;

import java.io.Serializable;
import java.util.List;

public class PTTripItem implements Serializable {
	public Boolean getIsUp() {
		return isUp;
	}
	public void setIsUp(Boolean isUp) {
		this.isUp = isUp;
	}
	public String getSpeedBandName() {
		return speedBandName;
	}
	public void setSpeedBandName(String speedBandName) {
		this.speedBandName = speedBandName;
	}
	public String getTrainType() {
		return trainType;
	}
	public void setTrainType(String trainType) {
		this.trainType = trainType;
	}
	public String getTrip() {
		return trip;
	}
	public void setTrip(String trip) {
		this.trip = trip;
	}
	public String getTripDate() {
		return tripDate;
	}
	public void setTripDate(String tripDate) {
		this.tripDate = tripDate;
	}
	public List<PTCallItem> getCalls() {
		return calls;
	}
	public void setCalls(List<PTCallItem> calls) {
		this.calls = calls;
	}
	
	private String trip;
	private String tripDate;
	private String trainType;
	private String speedBandName;
	private Boolean isUp;
	private List<PTCallItem> calls;
	
	
}
