package au.com.modis.etg.datacenter.transformer.fa;

import javax.xml.datatype.Duration;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.api.transport.PropertyScope;
import org.mule.transformer.AbstractMessageTransformer;

import com.quintiq.serviceplanner.fleetallocresp.FleetAllocationResponseMsg;

import au.com.modis.etg.common.utils.ETGDBDateUtil;
import au.com.modis.etg.datacenter.transformer.OutboundProperty;
import au.com.modis.etg.datacenter.transformer.SessionVars;
import au.com.modis.etg.datacenter.util.ETGDateConvertUtil;

public class FADBDtoToFAMessage extends AbstractMessageTransformer{

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		final DateTime targetDate = ETGDBDateUtil.parseFromDB(message.getProperty(SessionVars.TARGET_DATE, PropertyScope.SESSION));
		final DateTime baselineDate = ETGDBDateUtil.parseFromDB(message.getProperty(SessionVars.BASELINE_DATE, PropertyScope.SESSION));
		final String requestRef = message.getProperty(SessionVars.REQUEST_REF, PropertyScope.SESSION);
		
		FleetAllocationResponseMsg msg = ((FleetAllocationResponseMsg) message.getPayload());
		DateTime originalTime = DateTime.parse(msg.getHeader().getTimeStamp());
		msg.getHeader().setTimeStamp(
				originalTime.withDate(targetDate.getYear(), targetDate.getDayOfMonth(), targetDate.getDayOfMonth()).toString());
		
		// set request ref
		if (requestRef != null) {
			msg.getHeader().setCorrelationID(requestRef);
			message.setOutboundProperty(OutboundProperty.CORELATIONID, requestRef);
			message.setOutboundProperty(OutboundProperty.MESSAGEID, msg.getHeader().getMsgID());
		}
		msg.getBody().getFleetAllocResponse().getFleetAllocs().getFleetAlloc().forEach(fa -> {
			fa.setTripDate(ETGDBDateUtil.convertToTargetDate(fa.getTripDate(), targetDate, 
					ETGDateConvertUtil.differentDayWithBaseline(baselineDate, targetDate)));
		});
		message.setPayload(msg);
		return message;
	}

}
