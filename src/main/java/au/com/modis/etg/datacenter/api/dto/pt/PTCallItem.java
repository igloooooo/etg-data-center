package au.com.modis.etg.datacenter.api.dto.pt;

import java.io.Serializable;

import org.joda.time.DateTime;

public class PTCallItem implements Serializable {
	

    public DateTime getA() {
		return a;
	}
	public void setA(DateTime a) {
		this.a = a;
	}
	public DateTime getD() {
		return d;
	}
	public void setD(DateTime d) {
		this.d = d;
	}
	public String getS() {
		return s;
	}
	public void setS(String s) {
		this.s = s;
	}
	// arrival time
	private DateTime a;
	// departure time
    private DateTime d;
    // stopRef
    private String s;
    
    
}
